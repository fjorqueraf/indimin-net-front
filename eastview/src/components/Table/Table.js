import React from "react";
import { Table as TableAntd } from "antd";

import useMediaQuery from "../../hooks/useMediaQuery";

export default function Table(props) {
  const { columns, data, loadingTable } = props;


  const isMobile = useMediaQuery("max-width", 900);

  return (
    <TableAntd
      pagination={{ defaultPageSize: 5 }}
      loading={loadingTable}
      columns={columns}
      dataSource={data}
      style={{ width: "100%" }}
      scroll={{ x: isMobile ? 1200 : null }}
    />
  );
}
