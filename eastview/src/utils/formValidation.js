export function minLengthValidation(inputData, minLength) {
    const { value } = inputData;
  
    removeClassErrorSuccess(inputData);
  
    if (value.length >= minLength) {
      inputData.classList.add("success");
      return true;
    } else {
      inputData.classList.add("error");
      return false;
    }
  }
  
  export function emailValidation(inputData) {
    const emailValid = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    const { value } = inputData;
  
    removeClassErrorSuccess(inputData);
  
    const resultValidation = emailValid.test(value);
    if (resultValidation) {
      inputData.classList.add("success");
      return true;
    } else {
      inputData.classList.add("error");
      return false;
    }
  }
  
  export function formateaRut(rut) {
    var actual = rut.replace(/^0+/, "");
    if (actual != '' && actual.length > 1) {
      var sinPuntos = actual.replace(/\./g, "");
      var actualLimpio = sinPuntos.replace(/-/g, "");
      var inicio = actualLimpio.substring(0, actualLimpio.length - 1);
      var rutPuntos = "";
      var i = 0;
      var j = 1;
      for (i = inicio.length - 1; i >= 0; i--) {
        var letra = inicio.charAt(i);
        rutPuntos = letra + rutPuntos;
        if (j % 3 == 0 && j <= inicio.length - 1) {
          rutPuntos = "." + rutPuntos;
        }
        j++;
      }
      var dv = actualLimpio.substring(actualLimpio.length - 1);
      rutPuntos = rutPuntos + "-" + dv;
    }
    return rutPuntos;
  }
  
  export function rutValidation(inputData) {
    inputData = inputData.replace("‐", "-");
    inputData = inputData.split('.').join("");
    if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test(inputData)) return false;
    var tmp = inputData.split("-");
    var digv = tmp[1];
    var rut = tmp[0];
    if (digv == "K") digv = "k";
    if (dvValidatation(rut) == digv) {
  
  
      return true;
    } else {
  
  
      return false;
    }
  }
  
  function dvValidatation(T) {
    var M = 0,
      S = 1;
    for (; T; T = Math.floor(T / 10)) S = (S + (T % 10) * (9 - (M++ % 6))) % 11;
    return S ? S - 1 : "k";
  }
  
  export function phoneValidation(inputData) {
    const phoneValid = /^[0-9\+]{9,14}$/;
    const { value } = inputData;
  
    removeClassErrorSuccess(inputData);
  
    const resultValidation = phoneValid.test(value);
    if (resultValidation) {
      inputData.classList.add("success");
      return true;
    } else {
      inputData.classList.add("error");
      return false;
    }
  }
  
  export function textValidation(inputData) {
    const textValid = /^[a-zA-Z ]*$/;
    const { value } = inputData;
  
    removeClassErrorSuccess(inputData);
  
    const resultValidation = textValid.test(value);
    if (resultValidation) {
      inputData.classList.add("success");
      return true;
    } else {
      inputData.classList.add("error");
      return false;
    }
  }
  
  function removeClassErrorSuccess(inputData) {
    inputData.classList.remove("success");
    inputData.classList.remove("error");
  }
  
  export function isNullOrEmpty(value) {
    return (!value || value == null || value == undefined || value == "" || value.length == 0 || value == "-1");
  }
  
  export function validationPhono(fono) {
    var regExp = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    var phone = fono.replace(/\D/g, "");
    if (regExp.test(phone)) {
      return true;
    }
    return false;
  }
  
  export function validationEmail(email) {
    const emailValid = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    const value = email;
  
    const resultValidation = emailValid.test(value);
    if (resultValidation) {
      return true;
    }
    return false;
  }