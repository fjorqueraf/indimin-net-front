import moment from "moment";

export function formatDate(date, format) {
    let fechaFormateada = moment(date).format(format);
    return fechaFormateada;
}